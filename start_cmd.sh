podman kill palaestrai-demonstrator-submodules || true 
podman rm palaestrai-demonstrator-submodules || true 

#podman build -f docker/Dockerfile . -t palaestrai-demonstrator-submodules

podman run $(/opt/pkm/bin/pmgpu.sh) -e ROOTLESS=1 --name palaestrai-demonstrator-submodules -v /home/tlogemann/classic-arl-submodules/docker/demonstrator-start.sh:/carl/demonstrator-start.sh -v /home/tlogemann/classic-arl-submodules/docker/start.sh:/palaestrai/containers/start.sh  -v /home/tlogemann/classic-arl-submodules/docker/midas_data:/root/.config/midas/midas_data -v /home/tlogemann/classic-arl-submodules:/workspace palaestrai-demonstrator-submodules "/bin/bash /carl/demonstrator-start.sh" > output.log

