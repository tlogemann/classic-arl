SELECT id,
       walltime,
       agent_id,
       simtimes,
       rewards,
       actuator_id,
       cast((_setpoint::json ->> 'values')::json ->> 0 as double precision) as "actuator_setpoint",
       sensor_id,
       cast(sensor_value::json ->> 'value' as double precision) as "sensor_value"
FROM muscle_actions ma
         CROSS JOIN LATERAL
    jsonb_to_recordset(
            jsonb_path_query_array(
                    ma.sensor_readings,
                    '$[*] ? (@.sensor_id like_regex "vm_pu")'
                )
        ) as x(sensor_id TEXT, sensor_value jsonb)
         CROSS JOIN LATERAL
    jsonb_to_recordset(
            jsonb_path_query_array(
                    ma.actuator_setpoints,
                    '$[*] ? (@.actuator_id like_regex "q_set_mvar")'
                )
        ) as y(actuator_id TEXT, _setpoint jsonb)
where id = 1;