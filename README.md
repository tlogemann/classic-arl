# Classic ARL

Classic ARL -- where it all began. 
The original ARL scenario consisted of two reinforcement learning agents with contradicting goals.
The agents were acting on a power grid environment with almost full control. 
The environment itself used the technical connection rules (German: Technische Anschlussregeln, TAR) to keep itself in a healthy state.
One of the agents had the goal to assist the TAR and keep the grid in a healthy state while the other had to create some kind of attack.
Each of the agents had one indivual training phase and one joint training phase, followed by a joined test phase.

This repository aims to reproduce this scenario using the palaestrAI framework together with MIDAS as power grid environment.

## Clone submodules

Directly clone with the submodules checked out

```bash
git clone --recurse-submodules git@gitlab.com:tlogemann/classic-arl.git classic-arl-submodules
```

## Preparation

There is actually no coding required to run this scenario.
But you still need to set up a virtual environment and install a few packages.

This repository contains a requirements containing all necessary packages. 
You can install everything with:

```bash
pip install -r requirements.txt
```

## Configuring the Database

The analysis script is configured to use a local sqlite database, located in the root directory of this repository.
This is also the default configuration of a palaestrai runtime configuration file.
If you don't have one yet, navigate into the root directory of this repository and type:

```bash
palaestrai runtime-config-show-default > runtime.conf.yaml
```

To set up the database, type:

```bash
palaestrai database-create
```

If you want to use a different database, remember to change the analysis scripts as well.

## Running the Experiment

If you set up everything correctly, running the experiment is actually really simple.
Just type in another command:

```bash
arsenai generate palaestrai-experiment-files/carl_reference_experiment.yml
palaestrai start palaestrai-runfiles/Classic-ARL-Experiment_run-0.yml
```

This will start the *reference* experiment, with four phases and two agents with contradicting goals.

## Docker/Podman
The submodules have to be checked out.

**NOTE** To only execute `palaestrai-runfiles/simple_defender.yml`,
remove all other files from `palaestrai-runfiles` and from `palaestrai-experiment-files`

Build with the following

```bash
podman build -f docker/Dockerfile . -t palaestrai-demonstrator
```

Start with the following (for rootless environment)
Adjust the paths on the host

```bash
podman run -e ROOTLESS=1 --name palaestrai-demonstrator \
-v /home/tlogemann/classic-arl/docker/start.sh:/palaestrai/containers/start.sh \
-v /home/tlogemann/classic-arl:/workspace palaestrai-demonstrator:latest
```

Copy the downloaded midas-data and mount them for more speed while init the next container:

```bash
podman run -e ROOTLESS=1 --name palaestrai-demonstrator \
-v /home/tlogemann/classic-arl/docker/start.sh:/palaestrai/containers/start.sh \
-v /home/tlogemann/classic-arl/docker/midas_data:/root/.config/midas/midas_data \
-v /home/tlogemann/classic-arl:/workspace palaestrai-demonstrator:latest
```

## Analysis

You can do the analysis either in a jupyter notebook or directly with a python script.
If you haven't changed the database you can run the analysis (once the experiment has finished) with:

```bash
# May be outdated
cd analysis 
python carl4_analysis.py
```

This should generate a new directory `results` and a subdirectory with two plots (.png files) and a `metadata.txt`.

